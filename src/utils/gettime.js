function p(s) {
    return s < 10 ? "0" + s : s;
}

function gettime_start(time) {
    var end_time = new Date(time[0]);
    const end =
        p(end_time.getFullYear()) +
        "-" +
        p(end_time.getMonth() + 1) +
        "-" +
        p(end_time.getDate());
    return end
}

function gettime_end(time) {
    var end_time = new Date(time[1]);
    const end =
        p(end_time.getFullYear()) +
        "-" +
        p(end_time.getMonth() + 1) +
        "-" +
        p(end_time.getDate());
    return end
}

function singe_gettime_start(value) {
    var singe_gettime_start = new Date(value);
    const start_end =
        p(singe_gettime_start.getFullYear()) +
        "-" +
        p(singe_gettime_start.getMonth() + 1) +
        "-" +
        p(singe_gettime_start.getDate());
        return start_end
}


function gettime_time_end(time) {
    var end_time = new Date(time[1]);
    const end =
        p(end_time.getFullYear()) +
        "-" +
        p(end_time.getMonth() + 1) +
        "-" +
        p(end_time.getDate()) +
        " " +
        p(end_time.getHours()) +
        ":" +
        p(end_time.getMinutes()) +
        ":" +
        p(end_time.getSeconds());
    return end
}

function gettime_time_start(time) {
    var start_time = new Date(time[0]);
    const start =
        p(start_time.getFullYear()) +
        "-" +
        p(start_time.getMonth() + 1) +
        "-" +
        p(start_time.getDate()) +
        " " +
        p(start_time.getHours()) +
        ":" +
        p(start_time.getMinutes()) +
        ":" +
        p(start_time.getSeconds());
    return start
}
function formatDate(date, type, format) {
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	m = m < 10 ? '0' + m : m;
	var d = date.getDate();
	d = d < 10 ? ('0' + d) : d;
	var h = date.getHours();
	h = h < 10 ? ('0' + h) : h;
	var minute = date.getMinutes();
	minute = minute < 10 ? ('0' + minute) : minute;
	var s = date.getSeconds();
	s = s < 10 ? ('0' + s) : s;
	if(type == "date") {
		return y + '-' + m + '-' + d;
	} else if(type == "datetime") {
		if(format == 'start') {
			return y + '-' + m + '-' + d + ' 00:00:00';
		}
		else if(format == 'end') {
			return y + '-' + m + '-' + d + ' 23:59:59';
		}else{
			return y + '-' + m + '-' + d + ' '+h+':'+minute+':'+s;
		}
	}
}
export {
    gettime_start,
    gettime_end,
    gettime_time_start,
    gettime_time_end,
    formatDate,
    singe_gettime_start
}