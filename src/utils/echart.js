import echarts from 'echarts/lib/echarts'

import 'echarts/lib/chart/pie'
import 'echarts/lib/component/legend'

export default echarts
