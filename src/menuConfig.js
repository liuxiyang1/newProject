// 菜单配置
// headerMenuConfig：头部导航配置
// asideMenuConfig：侧边导航配置

const headerMenuConfig = [];

const asideMenuConfig = [
  {
    path: '/dashboard',
    name: '基本信息',
    icon: 'icon iconfont iconunsel6',
    // children: [
    //   {
    //     path: '/home',
    //     name: '基础详情',
    //   },
    //   {
    //     path: '/dashboard',
    //     name: '新增',
    //   },
    // ],
  },
  {
    path: '/reDashboard',
    name: '重写基本信息',
    icon: 'icon iconfont iconunsel6',
    // children: [
    //   {
    //     path: '/home',
    //     name: '基础详情',
    //   },
    //   {
    //     path: '/dashboard',
    //     name: '新增',
    //   },
    // ],
  },
  {
    path: '/HealthManagement',
    name: '健康管理',
    icon: 'icon iconfont iconunsel4',

  },
  {
    path: '/AttCheck',
    name: '考勤管理',
    icon: 'icon iconfont iconunsel1',

  }, {
    path: '/Visitorlogs',
    name: '访客记录',
    icon: 'icon iconfont iconunsel5',
  },
  {
    path: '/Records',
    name: '设备管理',
    icon: 'icon iconfont iconunsel',
    // children: [
    //   {
    //     path: '/success',
    //     name: '基础详情页',
    //   },
    //   {
    //     path: '/fail',
    //     name: '失败',
    //   },
    // ],
  },
];

export { headerMenuConfig, asideMenuConfig };
