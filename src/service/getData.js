import http from '../utils/axios'
//登录
export const login = (params) => http.post('/v1/account/login', params);
//录入基本信息
// export const Information = (params) => http.post('/v1/account/Information', params);
//访客列表
export const list = (params) => http.post('/v1/visitor/list', params);
//访客列表 导出
export const listExport = (params) => http.post('/v1/visitor/export', params, 'blob');
//考勤管理
export const userlist = (params) => http.post('/v1/user/list', params);
//健康管理视图模式
export const healthviewSum = (params) => http.post('/v1/health/viewSum', params);
//健康管理列表模式
export const healthlist = (params) => http.post('/v1/health/list', params);
//访客登记
export const visitorcheck = (params) => http.post('/v1/visitor/check', params);
//人员录入编辑
export const usercheck = (params) => http.post('/v1/user/check', params);
//基本信息列表
export const userInfoList = (params) => http.post('/v1/user/userInfoList', params);
//基本信息删除
export const userdelete = (params) => http.post('/v1/user/delete', params);
//设备信息删除
export const devicedelete = (params) => http.post('/v1/device/delete', params);
//设备列表
export const devicelist = (params) => http.post('/v1/device/list', params);
//新增设备
export const devicesave = (params) => http.post('/v1/device/save', params);
//异常设备
export const offlineAlertList = (params) => http.post('/v1/device/offlineAlertList', params);
//设备统计
export const countStatus = (params) => http.post('/v1/device/countStatus', params);
//考勤列表导出
export const userexport = (params) => http.post('/v1/user/export', params, 'blob');
//列表导出
export const healthexport = (params) => http.post('/v1/health/export', params, 'blob');

// export const healthexport = (params) => http.post('/v1/health/exportToUrl', params);

//杭七中上传oss
export const importstudent = (params) => http.post('/v1/import/student', params);
//账号管理列表
export const accountList = (params) => http.post('/v1/account/accountList', params);
//删除账号
export const accountdelete = (params) => http.post('/v1/account/delete', params);
//新增账号
export const accountsave = (params) => http.post('/v1/account/save', params);
//修改账号
export const accountupdate = (params) => http.post('/v1/account/update', params);
//湖州学校扫码
export const studentscanSave = (params) => http.post('v1/student/scanSave', params);

export const studentSaveStatus = (params) => http.post('v1/student/studentSaveStatus', params);
//学生管理
export const studentmessage = (params) => http.post('/v1/student/checkRecordList', params);

export const studentregister = (params) => http.post('/v1/student/punchRecordList', params);

//学生管理列表导出
export const studentexport = (params) => http.post('/v1/student/exportToUrl', params);

export const studentExport2 = (params) => http.post('/v1/student/export', params, 'blob');
//学生打卡导出
export const registerexport = (params) => http.post('/v1/student/exportPunchRecord', params, 'blob');

