// 以下文件格式为描述路由的协议格式
// 你可以调整 routerConfig 里的内容
// 变量名 routerConfig 为 iceworks 检测关键字，请不要修改名称
const HeaderAsideLayout = () => import('./layouts/HeaderAsideLayout');
//基本信息
const Dashboard = () => import('./pages/Dashboard');

//重写基本信息
const reDashboard = () => import('./pages/reDashboard');
//健康管理
const HealthManagement = () => import('./pages/HealthManagement');
//访客记录
const Visitorlogs = () => import('./pages/Visitorlogs');
const NotFound = () => import('./pages/NotFound');
//登录
const Login = () => import('./pages/Login')
//
//考勤管理
const AttCheck = () => import('./pages/AttCheck');

//信息录入移动端
const FillIn = () => import('./pages/FillIn')
//信息录入移动端
const Records = () => import('./pages/Records')
//信息登记移动端
const Registration = () => import('./pages/Registration')
//oss上传文件
const UploadOSSimage = () => import('./pages/UploadOSSimage');
//账号管理
// const AccountManagement = () => import('./pages/AccountManagement');
//湖州学校扫码登记3
const SchoolRegistration = () => import('./pages/SchoolRegistration');
// //湖州学校扫码登记 1
const Manage = () => import('./pages/SchoolRegistration/manage');

// //湖州学校扫码登记 2
const SchoolRegQuery = () => import('./pages/SchoolRegistration/SchoolRegQuery');
//学生管理
// const student = () => import('./pages/student');
// const studentdetail = () => import('./pages/student/studentdetail/studentmessage');
// const studentregister = () => import('./pages/student/studentregister/studentregister');

//学生健康信息
// const Studenthealth = () => import('./pages/Studenthealth');
const routerConfig = [
  {
    path: '/',
    layout: HeaderAsideLayout,
    component: Dashboard,
    meta: {
      // 添加该字段，表示进入这个路由是需要登录的
      requireAuth: true,
    },
    children: [
      {
        //信息录入
        // path: '/dashboard/analysis',
        path: '/dashboard',
        layout: HeaderAsideLayout,
        component: Dashboard,
        meta: {
          // 添加该字段，表示进入这个路由是需要登录的
          requireAuth: true,
        },
      },
    ],
  },

  {
    //重写基本管理
    path: '/reDashboard',
    layout: HeaderAsideLayout,
    component: reDashboard,
    meta: {
      // 添加该字段，表示进入这个路由是需要登录的
      requireAuth: true,
    },
    children: [
      {
        path: '/reDashboard',
        layout: HeaderAsideLayout,
        component: reDashboard,
        meta: {
          // 添加该字段，表示进入这个路由是需要登录的
          requireAuth: true,
        },
      },
    ],
  },
  {
    //健康管理
    path: '/HealthManagement',
    layout: HeaderAsideLayout,
    component: HealthManagement,
    meta: {
      // 添加该字段，表示进入这个路由是需要登录的
      requireAuth: true,
    },
    children: [
      {
        path: '/HealthManagement',
        layout: HeaderAsideLayout,
        component: HealthManagement,
        meta: {
          // 添加该字段，表示进入这个路由是需要登录的
          requireAuth: true,
        },
      },
    ],
  },


  {
    //湖州学校扫码登记
    path: '/SchoolRegistration',
    layout: SchoolRegistration,
    component: SchoolRegistration,
  },
  {
    //湖州学校扫码登记
    path: '/Manage',
    layout: Manage,
    component: Manage,
  },
  {
    //湖州学校扫码登记
    path: '/SchoolRegQuery',
    layout: SchoolRegQuery,
    component: SchoolRegQuery,
  },
  {
    //访客记录
    path: '/Visitorlogs',
    layout: HeaderAsideLayout,
    component: Visitorlogs,
    children: [
      {
        path: '/Visitorlogs',
        layout: HeaderAsideLayout,
        component: Visitorlogs,
      },
    ],
    meta: {
      // 添加该字段，表示进入这个路由是需要登录的
      requireAuth: true,
    },
  },
  {
    path: '/AttCheck',
    layout: HeaderAsideLayout,
    component: AttCheck,
    children: [
      {
        path: '/AttCheck',
        layout: HeaderAsideLayout,
        component: AttCheck,
      },
    ], meta: {
      // 添加该字段，表示进入这个路由是需要登录的
      requireAuth: true,
    },
  },
  {
    path: '/Records',
    layout: HeaderAsideLayout,
    component: Records,
    children: [
      {
        path: '/Records',
        layout: HeaderAsideLayout,
        component: Records,
      },
    ],
  },
  {
    path: '/result',
    layout: HeaderAsideLayout,
    component: NotFound,
    children: [
      {
        path: '/result/success',
        layout: HeaderAsideLayout,
        component: NotFound,
      },
      {
        path: '/result/fail',
        layout: HeaderAsideLayout,
        component: NotFound,
      },
    ],
  },
  {
    path: '/login',
    layout: Login,
    component: Login,
    // meta: {
    //   icon: '',
    //   keepAlive: true,
    //   title: '用户登录'
    // }
  },
  {
    path: '/FillIn',
    layout: FillIn,
    component: FillIn,
  },
  {
    path: '/Registration',
    layout: Registration,
    component: Registration,
  },
  {
    path: '/UploadOSSimage',
    layout: UploadOSSimage,
    component: UploadOSSimage,
  },
  {
    path: '/SchoolRegistration',
    layout: SchoolRegistration,
    component: SchoolRegistration,
  },
  {
    path: '*',
    layout: HeaderAsideLayout,
    component: NotFound,

  },
];

/**
 * meta 可配置参数
 * @param {boolean} icon 页面icon
 * @param {boolean} keepAlive 是否缓存页面
 * @param {string} title 页面标题
 */

export default routerConfig;
