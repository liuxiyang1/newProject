import Vue from 'vue';
import ELEMENT from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
import 'normalize.css/normalize.css';
import './styles/index.scss';

import App from './App.vue';
import router from './router';
import './assets/js/rem'
import './assets/css/reset.css'
import './assets/css/main.css'
import './assets/icon/iconfont.css'
import './assets/icon/iconfont.js'



// 全局引入elementui组件

// import { Row, Col, Button, Popup, Field, NumberKeyboard, ImagePreview, Picker, ActionSheet, Cell, CellGroup, Uploader, Dialog } from 'vant'
import BaiduMap from 'vue-baidu-map'
Vue.use(BaiduMap, {
  ak: ''
})



Vue.use(ELEMENT);
// Vue.use(MintUI);
// Vue.use(Row).use(Col).use(Button).use(Popup).use(Field).use(NumberKeyboard).use(ImagePreview).use(Picker).use(ActionSheet).use(Cell).use(CellGroup).use(Uploader).use(Dialog);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
