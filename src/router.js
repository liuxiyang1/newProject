import Vue from 'vue';
import Router from 'vue-router';
import routerConfig from './routerConfig';

/**
 * 将路由配置扁平化
 * @param {Array} config 路由配置
 *
 * @return {Route}
 * @example
 //  * const routes = [
 //  *   {
//  *     path: '/dashboard/analysis',
//  *     component: HeaderAsideLayout,
//  *     children: [
//  *       {
//  *         path: '',
//  *         component: Dashboard,
//  *       },
//  *     ],
//  *   },
 //  * ];
 */
const routerMap = [];

const recursiveRouterConfig = (config = []) => {
  config.forEach((item) => {
    const route = {
      path: item.path,
      component: item.layout,
      meta:item.meta,
      children: [
        {
          path: '',
          component: item.component,
        },
      ],
    };

    if (Array.isArray(item.children)) {
      recursiveRouterConfig(item.children);
    }
    routerMap.push(route);
  });

  return routerMap;
};

const routes = recursiveRouterConfig(routerConfig);

// Vue.use(Router);


const router = new Router({
  // mode: 'history',
  routes
});


const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location)
}


router.beforeEach((to, from, next) => {
  if(to.matched.some((r) => r.meta.requireAuth)) {

    if(sessionStorage.getItem("accountId") != null && sessionStorage.getItem("accountId") != '') { //判断是否已经登录
      next();
    } else {
      next({
        path: '/Login',
//				query: {
//					redirect: to.fullPath
//				}
        //登录成功后重定向到当前页面
      });
    }
  } else {
    next();
  }
  document.body.scrollTop = 0
  // firefox
  document.documentElement.scrollTop = 0
  // safari
  window.pageYOffset = 0
})

export default router


