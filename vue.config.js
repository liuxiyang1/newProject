var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  publicPath: './',
  lintOnSave: false,
  configureWebpack: (config) => {
    if (process.env.NODE_ENV === 'production') {
      // 为生产环境修改配置...
      config.mode = 'production'
      // 将每个依赖包打包成单独的js文件
      let optimization = {
        runtimeChunk: 'single',
        splitChunks: {
          chunks: 'all',
          maxInitialRequests: Infinity,
          minSize: 20000,
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                // get the name. E.g. node_modules/packageName/not/this/part.js
                // or node_modules/packageName
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
                // npm package names are URL-safe, but some servers don't like @ symbols
                return `npm.${packageName.replace('@', '')}`
              }
            }
          }
        }
      }
      // config.plugins = config.plugins.concat(
      //     [
      //       new HtmlWebpackPlugin({
      //         inject: 'body'
      //       })
      //     ]
      // )
      Object.assign(config, {
        optimization
      })
    } else {
      // 为开发环境修改配置...
      config.mode = 'development'
    }
    Object.assign(config, {
      externals: {
        'vue': 'Vue',
        'vue-router': 'VueRouter',
        'element-ui': 'ELEMENT'
      },
    })
  },
  productionSourceMap: false,
  baseUrl: './', //路径基础地址
  outputDir: 'dist', // 打包输出地址
  assetsDir: 'static', // 静态资源基础地址
  devServer: {
    // 设置主机地址
    host: '0.0.0.0', // 本地端口配置
    // 设置默认端口
    port: 8080, // 本地端口配置
    // 设置代理
    proxy: {
      '/api': {
        // 目标 API 地址
        target: 'http://192.168.5.106:8085', //代理配置
        changeOrigin: true,// 将主机标头的原点更改为目标URL
        // ws: true,// 是否启用websockets
        pathRewrite: {
          '^/api': '/'
        }
      }
    },
    disableHostCheck: true
  },
};
